
The webcomic module is designed to maintain an online comic or graphic novel. It can be used for simple strip-a-day comics as well as more complex graphic novels, where organization by storyline and issue are essential.

FEATURES
--------
* Maintain bios of every recurring character
* List them on a 'cast' page, and link them to each strip they appear in
* Organize strips by publish date, strip number, issue/episode, storyline, etc
* Scheduled publishing of individual strips
* A convenient 'in-story timestamp' for each strip, to maintain continuity
* Customizable archiving and browsing
* Customizable commenting can point readers to your forum to discuss a strip

TODO
----
Basic content types (strip, episode, storyline, character) -- DONE
Image manipulation (copy/paste/hack from image.module, with help from audio.module) -- FUNCTIONAL, NEEDS SEPARATE TABLES
Settings/customization -- FUNCTIONAL, NEEDS IMPROVEMENT
Scheduled publishing -- DONE
Browsing system (copy/paste/hack from book.module -- FUNCTIONAL, NEEDS IMPROVEMENT
Archive modes (default views, via views.module) -- FUNCTIONAL, NEEDS IMPROVEMENT
Archive browsing/summaries (theming functions for the default views) -- STRIP-BASED VIEW DONE
Auto-creation of forum topics for each published episode or storyline
Latest Comic view, customizable. (Show author per-strip, per-day, latest, etc)