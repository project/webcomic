<?php

/**
 * @file
 * This include file implements views functionality for webcomic.module
 */

function webcomic_views_tables() {
  $tables['webcomic_content'] = array(
    'name' => 'webcomic_content',
    'provider' => 'internal', // won't show up in external list.
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'nid'
      ),
    ),
    'fields' => array(
      'publish_timestamp' => array(
        'name' => t('Webcomic: Publish date'),
        'sortable' => true,
        'handler' => array(
          'views_handler_field_date_small'    => t('As Short Date'),
          'views_handler_field_date'          => t('As Medium Date'),
          'views_handler_field_date_large'    => t('As Long Date'),
          'webcomic_views_handler_field_diff' => t('As Time Ago/Until'),
        ),
        'help' => t('Display the date a webcomic node was (or will be) published on.'),
      ),
      'storyline_timestamp' => array(
        'name' => t('Webcomic: Storyline date'),
        'sortable' => true,
        'handler' => array(
          'views_handler_field_date_small'    => t('As Short Date'),
          'views_handler_field_date'          => t('As Medium Date'),
          'views_handler_field_date_large'    => t('As Long Date'),
          'webcomic_views_handler_field_diff' => t('As Time Ago/Until'),
        ),
        'help' => t('Display the date a webcomic node takes place.'),
      ),
      'weight' => array(
        'name' => t('Webcomic: Page/Episode number'),
        'sortable' => true,
        'help' => t('Display the storyline/issue/page/etc number of a webcomic node.'),
      ),
      'sequence' => array(
        'name' => t('Webcomic: Sequence number'),
        'sortable' => true,
        'help' => t('Display the internal sorting number of the webcomic node.'),
      ),
    ),
    'sorts' => array(
      'publish_timestamp' => array(
        'name' => t('Webcomic: Publish date'),
        'help' => t('This allows you to sort by the date a webcomic node was (or will be) published on.'),
      ),
      'storyline_timestamp' => array(
        'name' => t('Webcomic: Storyline date'),
        'help' => t('This allows you to sort by the date a webcomic node takes place.'),
      ),
      'weight' => array(
        'name' => t('Webcomic: Page/Episode number number'),
        'help' => t('This allows you to sort by the storyline/issue/page/etc number of a webcomic node.'),
      ),
      'sequence' => array(
        'name' => t('Webcomic: Sequence number'),
        'help' => t('This allows you to sort by the internal sequence number of a webcomic node.'),
      ),
    ),
  );
  
    $tables['webcomic_parent'] = array(
    'name' => 'webcomic_parent',
    'provider' => 'internal',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid'
      ),
      'right' => array(
        'field' => 'child_nid'
      )
    ),
    "filters" => array(
      'parent' => array(
         'name' => t('Webomic: Parent Node'),
         'list' => 'views_handler_filter_webcomic_parent_zero',
         'operator' => 'views_handler_operator_andor',
         'help' => t('This allows you to filter webcomics based on parent node.'),
      ),
    ),
  );
  $tables['webcomic_parent_node'] = array(
    'name' => 'node',
    'provider' => 'internal',
    'join' => array(
      'left' => array(
        'table' => 'webcomic_parent',
        'field' => 'parent_nid'
      ),
      'right' => array(
        'field' => 'nid'
      )
    ),
    'fields' => array(
      'title' => array(
        'name' => t('Webcomic: Parent Title'),
        'handler' => array(
          'views_handler_field_webcomic_parent_title'    => t('As Link'),
          'views_handler_field_webcomic_parent_title_nl' => t('Without Link'),
        ),
        'addlfields' => array('nid'),
        'help' => t('Display the title of the parent node'),
      ),
    ),
    'sorts' => array(
      'title' => array(
        'name' => t('Webcomic: Parent Title'),
        'help' => t('Sort by the title of the parent node'),
      ),
    ),
  );

  return $tables;
}


function webcomic_views_arguments() {
  $arguments = array(
    'webcomic_parent' => array(
      'name' => t("Webcomic: Parent Node ID"), 
      'handler' => "views_handler_arg_webcomic_parent",
    ),
  );
  
  return $arguments;
}

function views_handler_arg_webcomic_parent($op, &$query, $arg_type, $arg = '') {
  switch ($op) {
    case 'summary' :
      $query->ensure_table("webcomic_parent_node");
      $query->add_field("nid");
      $query->add_field("parent", "webcomic_parent");
      $query->add_field("title", "webcomic_parent_node");
      $query->add_field("nid", "webcomic_parent_node", "pnid");
      $query->add_where("webcomic_parent_node.nid IS NOT NULL");
      $field_info['field'] = "webcomic_parent_node.title";
      return $field_info;
      break;
    case 'sort':
      $query->add_orderby('webcomic_parent_node', 'title', $arg_type);
      break;
    case 'filter' :
      $query->ensure_table("webcomic_parent");
      $query->add_where("webcomic_parent.parent_nid = '$arg'");
      break;
    case 'link' :
      return l($query->title, "$arg/$query->pnid");
    case 'title' :
      if ($query) {
        $term = db_fetch_object(db_query("SELECT title FROM {node} WHERE nid = '%d'", $query));
        return $term->title;
      }
  }
}

/*
 * Format a field as a link to the webcomic parent node
 */
function views_handler_field_webcomic_parent_title($field_info, $field_data, $value, $data) {
  return l($value, "node/$data->webcomic_parent_node_nid");
}

function views_handler_field_webcomic_parent_title_nl($field_info, $field_data, $value, $data) {
  return check_plain($value);
}

function views_handler_filter_webcomic_parent_zero() {
  $parents = array();
  $result = db_query("SELECT DISTINCT parent_nid FROM {webcomic_parent} ORDER BY parent_nid");
  while ($obj = db_fetch_object($result)) {
    $parents[$obj->parent_nid] = "$obj->parent_nid";
  }
  
  return $parents;
}

/*
/**
 * Format a date as "X time ago/until".
 */
function webcomic_views_handler_field_diff($field_info, $field_data, $value, $data) {
  if ($value == 0) {
    return '';
  }

  $current = time();
  if ($current > $value) {
    return format_interval($current - $value) .' ago';
  }
  else {
    return format_interval($value - $current) .' from now';
  }
}

function webcomic_views_default_views() {
  $view = new stdClass();
  $view->name = 'strip_archive';
  $view->description = 'Strips in a simple list';
  $view->access = array ();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'list';
  $view->url = 'strips';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '50';
  $view->sort = array (
    array (
      'tablename' => 'webcomic_content',
      'field' => 'sequence',
      'sortorder' => 'ASC',
      'options' => '',
    ),
  );
  $view->argument = array ();
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => '',
      'handler' => 'views_handler_field_nodelink_with_mark',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (0 => 'webcomic_strip',),
    ),
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
  );
  $view->requires = array(webcomic_content, node);
  $views[$view->name] = $view;
  
  $view = new stdClass();
  $view->name = 'latest';
  $view->description = 'latest webcomic';
  $view->access = array ();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Latest comic';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'node';
  $view->url = 'latest';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '1';
  $view->block = TRUE;
  $view->block_title = 'Latest comic';
  $view->block_header = '';
  $view->block_header_format = '1';
  $view->block_footer = '';
  $view->block_footer_format = '1';
  $view->block_empty = '';
  $view->block_empty_format = '1';
  $view->block_type = 'teaser';
  $view->nodes_per_block = '1';
  $view->block_more = '1';
  $view->block_use_page_header = FALSE;
  $view->block_use_page_footer = FALSE;
  $view->block_use_page_empty = FALSE;
  $view->sort = array (
    array (
      'tablename' => 'webcomic_content',
      'field' => 'sequence',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array ();
  $view->field = array ();
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (0 => 'webcomic_strip',),
    ),
  );
  $view->requires = array(webcomic_content, node);
  $views[$view->name] = $view;

  $view = new stdClass();
  $view->name = 'cast';
  $view->description = 'shows all the characters in a webcomic';
  $view->access = array ();
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Cast of characters';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'teaser';
  $view->url = 'cast';
  $view->use_pager = FALSE;
  $view->nodes_per_page = '99';
  $view->sort = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'sortorder' => 'ASC',
      'options' => '',
    ),
  );
  $view->argument = array ();
  $view->field = array ();
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (0 => 'webcomic_character',),
    ),
  );
  $view->requires = array(node);
  $views[$view->name] = $view;

  return $views;
}
