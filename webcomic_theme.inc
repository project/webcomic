<?php

/**
 * @file
 * Theme functions for webcomic.module. This is the stuff you'll probably want to override in your own themes.
 */

/**
 * Theme a teaser
 *
 * @ingroup themeable
 */
function theme_webcomic_teaser($node) {
  return l(image_display($node, 'thumbnail'), 'node/'. $node->nid, array(), NULL, NULL, FALSE, TRUE) . $node->teaser;
}

/**
 * Theme a body
 *
 * @ingroup themeable
 */
function theme_webcomic_body($node, $size) {
  return image_display($node, $size) . $node->body;
}

/**
 * Prepares the links to children (TOC) and forward/backward
 * navigation for a node presented as a webcomic page.
 *
 * @ingroup themeable
 */
function theme_webcomic_navigation($node) {
  $output = '<div class="webcomic-navigation">';
  $output .= theme('webcomic_first_link', $node);
  $output .= theme('webcomic_prev_link', $node);
  $output .= theme('webcomic_index_link', $node);
  $output .= theme('webcomic_next_link', $node);
  $output .= theme('webcomic_last_link', $node);
  $output .= '</div>';

  return $output;
}

/**
 * Theme first link
 *
 * @ingroup themeable
 */
function theme_webcomic_first_link($node) {
  if ($first = webcomic_first($node)) {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-first.png';
    drupal_add_link(array('rel' => 'first', 'href' => url('node/'. $first->nid)));
    return l('<img src="'. $path .'" title="'. t('first') .'" />', 'node/'. $first->nid, array('class' => 'page-first', 'title' => $first->title), NULL, NULL, FALSE, TRUE);
  }
  else {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-first-dim.png';
    return '<img src="'. $path .'" />';
  }
}

/**
 * Theme prev link
 *
 * @ingroup themeable
 */
function theme_webcomic_prev_link($node) {
  if ($prev = webcomic_prev($node)) {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-previous.png';
    drupal_add_link(array('rel' => 'prev', 'href' => url('node/'. $prev->nid)));
    return l('<img src="'. $path .'" title="'. t('previous') .'" />', 'node/'. $prev->nid, array('class' => 'page-prev', 'title' => $prev->title), NULL, NULL, FALSE, TRUE);
  }
  else {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-previous-dim.png';
    return '<img src="'. $path .'" />';
  }
}

/**
 * Theme index link
 *
 * @ingroup themeable
 */
function theme_webcomic_index_link($node) {
  $path = drupal_get_path('module', 'webcomic') .'/img/go-up.png';
  drupal_add_link(array('rel' => 'index', 'href' => webcomic_index()));
  
  return l('<img src="'. $path .'" title="'. t('index') .'" />', webcomic_index(), array('class' => 'page-up', 'title' => t('Go to index page')), NULL, NULL, FALSE, TRUE);
}

/**
 * Theme next link
 *
 * @ingroup themeable
 */
function theme_webcomic_next_link($node) {
  if ($next = webcomic_next($node)) {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-next.png';
    drupal_add_link(array('rel' => 'next', 'href' => url('node/' . $next->nid)));
    return l('<img src="'. $path .'" title="'. t('next') .'" />', 'node/'. $next->nid, array('class' => 'page-next', 'title' => $next->title), NULL, NULL, FALSE, TRUE);
  }
  else {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-next-dim.png';
    return '<img src="'. $path .'" />';
  }
}

/**
 * Theme last link
 *
 * @ingroup themeable
 */
function theme_webcomic_last_link($node) {
  if ($latest = webcomic_last($node)) {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-last.png';
    drupal_add_link(array('rel' => 'last', 'href' => url('node/' . $latest->nid)));
    return l('<img src="'. $path .'" title="'. t('latest') .'" />', 'node/'. $latest->nid, array('class' => 'page-latest', 'title' => $latest->title), NULL, NULL, FALSE, TRUE);
  } else {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-last-dim.png';
    return '<img src="'. $path .'" />';
  }
}

function _webcomic_is_new_value($value) {
  static $last;
  if ($value != $last) {
    $last = $value;
    return $value;
  }
  else {
    $path = drupal_get_path('module', 'webcomic') .'/img/go-first-dim.png';
    return '<img src="'. $path .'" title="'. t('first') .'" />';
  }
}

/**
 * Theme strip archive
 *
 * @ingroup themeable
 */
function theme_views_view_strip_archive($view, $type, $nodes) {
  if ($type == 'page') {
    $output = '';

    // we should only really be switching on these four.
    switch (variable_get('webcomic_archive_mode', 'strips_month')) {
      case 'strips':
        foreach ($nodes as $nid) {
          $node = node_load($nid->nid);
          $list[] = theme('webcomic_strip_listing', $node);
        }
        $output .= theme('node_list', $list);
        break;

      case 'strips_month':
        foreach ($nodes as $nid) {
          $node = node_load($nid->nid);
          $month = _webcomic_is_new_value(format_date($node->publish_timestamp, 'custom', 'F Y'));
          if ($month) {
            if (is_array($list)) {
              $output .= theme('node_list', $list);
              $list = array();
            }
            $output .= '<h3 class="date_header">'. $month .'</h3>';
          }
          $list[] = theme('webcomic_strip_listing', $node);
        }
        $output .= theme('node_list', $list);
        break;

      case 'strips_year':
        foreach ($nodes as $nid) {
          $node = node_load($nid->nid);
          $year = _webcomic_is_new_value(format_date($node->publish_timestamp, 'custom', 'Y'));
          if ($year) {
            if (is_array($list)) {
              $output .= theme('node_list', $list);
              $list = array();
            }
            $output .= '<h3 class="date_header">'. $year .'</h3>';
          }
          $list[] = theme('webcomic_strip_listing', $node);
        }
        $output .= theme('node_list', $list);
        break;

      case 'strips_episode':
        foreach ($nodes as $nid) {
          $node = node_load($nid->nid);
          $episode_nid = _webcomic_is_new_value($node->parent_nid);
          if ($episode_nid) {
            if (is_array($list)) {
              $output .= theme('node_list', $list);
              $list = array();
            }
            $episode = node_load($episode_nid);
            $output .= theme('webcomic_episode_grouping', $episode);
          }
          $list[] = theme('webcomic_strip_listing', $node);
        }
        $output .= theme('node_list', $list);
        break;
    }
    
    return "<div class='archive'>$output</div>\n";
  }
  else {
    return theme('views_view', $view, $type, $nodes);
  }
}

/*
 * Formatting functions for the line-listing and summary header
 * views of the assorted node types.
 */

/**
 * Theme strip listing
 *
 * @ingroup themeable
 */
function theme_webcomic_strip_listing($node) {
  return l($node->title, 'node/'. $node->nid);
}

/**
 * Theme episode listing
 *
 * @ingroup themeable
 */
function theme_webcomic_episode_listing($node) {
  return l($node->title, 'node/'. $node->nid);
}

/**
 * Theme episode grouping
 *
 * @ingroup themeable
 */
function theme_webcomic_episode_grouping($node) {
  $output .= '<h3 class="episode_header">'. $node->title .'</h3>';
  $output .= '<p class="episode_teaser">'. $node->teaser .'</p>';
  
  return $output;
}

/**
 * Theme storyline listing
 *
 * @ingroup themeable
 */
function theme_webcomic_storyline_listing($node) {
  return l($node->title, 'node/'. $node->nid);
}

/**
 * Theme storyline grouping
 *
 * @ingroup themeable
 */
function theme_webcomic_storyline_grouping($node) {
  $output .= '<h3 class="storyline_header">'. $node->title .'</h3>';
  $output .= '<p class="storyline_teaser">'. $node->teaser .'</p>';
  
  return $output;
}
